#冷风博客系统 
#演示http://www.coldfeng.com/
#本系统是仿照wordpress的结构编写的一款asp.net mvc4是程序，本系统采用了MVC4框架,使用了LINQ操作数据库，

主要功能：

1.文章管理

2.会员管理

3.广告管理系统

4.留言板，

5.自定义标签。

6.友情连接
#附加数据库SQL Server 2008

（1）将文件夹中的Coldfeng_blogs文件拷贝到SQL Server 2008安装路径下的MSSQL.1MSSQLData目录下。

（2）选择开始/程序/Microsoft SQL Server 2008/SQL Server Management Studio项，进入到“连接到服务器”

（3）在“服务器名称”下拉列表中选择SQL Server 2008服务器名称，然后单击【连接】按钮。

（4）在“对象资源管理器”中右键单击“数据库”节点，在弹出的菜单中选择“附加”项，弹出“附加数据库”对话

 

（5）单击【添加】按钮，在弹出的“定位数据库文件”对话框中选择数据库文件路径。

（6）依次单击【确定】按钮，完成数据库附加操作。

注意：如果您的机器没有SQL Server 2008 本程序还附带了SQL Server 2008数据库。此数据存放位置与SQL Server 2008数据库在同一文件夹中。